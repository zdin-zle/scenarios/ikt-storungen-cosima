//
// Generated file, do not edit! Created by nedtool 5.6 from messages/AttackEvent.msg.
//

#ifndef __ATTACKEVENT_M_H
#define __ATTACKEVENT_M_H

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wreserved-id-macro"
#endif
#include <omnetpp.h>

// nedtool version check
#define MSGC_VERSION 0x0506
#if (MSGC_VERSION!=OMNETPP_VERSION)
#    error Version mismatch! Probably this file was generated by an earlier version of nedtool: 'make clean' should help.
#endif



class AttackEvent;
// cplusplus {{
#include "AttackType_m.h"
// }}

/**
 * Class generated from <tt>messages/AttackEvent.msg:21</tt> by nedtool.
 * <pre>
 * packet AttackEvent
 * {
 *     int attackType \@enum(AttackType);
 *     string attacked_module;
 *     int start;
 *     int stop;
 *     double probability;
 * }
 * </pre>
 */
class AttackEvent : public ::omnetpp::cPacket
{
  protected:
    int attackType = 0;
    omnetpp::opp_string attacked_module;
    int start = 0;
    int stop = 0;
    double probability = 0;

  private:
    void copy(const AttackEvent& other);

  protected:
    // protected and unimplemented operator==(), to prevent accidental usage
    bool operator==(const AttackEvent&);

  public:
    AttackEvent(const char *name=nullptr, short kind=0);
    AttackEvent(const AttackEvent& other);
    virtual ~AttackEvent();
    AttackEvent& operator=(const AttackEvent& other);
    virtual AttackEvent *dup() const override {return new AttackEvent(*this);}
    virtual void parsimPack(omnetpp::cCommBuffer *b) const override;
    virtual void parsimUnpack(omnetpp::cCommBuffer *b) override;

    // field getter/setter methods
    virtual int getAttackType() const;
    virtual void setAttackType(int attackType);
    virtual const char * getAttacked_module() const;
    virtual void setAttacked_module(const char * attacked_module);
    virtual int getStart() const;
    virtual void setStart(int start);
    virtual int getStop() const;
    virtual void setStop(int stop);
    virtual double getProbability() const;
    virtual void setProbability(double probability);
};

inline void doParsimPacking(omnetpp::cCommBuffer *b, const AttackEvent& obj) {obj.parsimPack(b);}
inline void doParsimUnpacking(omnetpp::cCommBuffer *b, AttackEvent& obj) {obj.parsimUnpack(b);}

#endif // ifndef __ATTACKEVENT_M_H

